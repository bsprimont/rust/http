
/** Linux */
#[cfg(target_os = "linux")]
mod linux_server;

#[cfg(target_os = "linux")]
pub struct Server
{
    pub port: u16,
    pub process_request_callback: fn(crate::Request) -> crate::Response,
    linux_server: linux_server::LinuxServer
}

#[cfg(target_os = "linux")]
impl Server {
    pub fn new(port: u16, process_request_callback: fn(crate::Request) -> crate::Response) -> Server {
        Server {
            port: port,
            process_request_callback: process_request_callback,
            linux_server: linux_server::LinuxServer::new(port, process_request_callback)
        }
    }

    pub fn start(&mut self) {
        self.linux_server.start();
    }
}

/** Windows */
#[cfg(target_os = "windows")]
mod windows_server;

#[cfg(target_os = "windows")]
pub struct Server
{
    pub port: u16,
    pub process_request_callback: fn(crate::Request) -> crate::Response,
    windows_server: windows_server::WindowsServer
}

#[cfg(target_os = "windows")]
impl Server {
    pub fn new(port: u16, process_request_callback: fn(crate::Request) -> crate::Response) -> Server {
        Server {
            port: port,
            process_request_callback: process_request_callback,
            windows_server: windows_server::WindowsServer::new(port, process_request_callback)
        }
    }

    pub fn start(&mut self) {
        self.windows_server.start();
    }
}
