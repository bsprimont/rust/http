///
/// HTTP server logic for the linux platform.
/// Uses the epoll syscall.
/// 

use std::io;
use log;

pub struct LinuxServer
{
    pub port: u16,
    pub process_request_callback: fn(crate::Request) -> crate::Response,
    pub socket_fd: i32,
    pub socket_backlog: i32
}

impl LinuxServer {
    pub fn new(port: u16, process_request_callback: fn(crate::Request) -> crate::Response) -> LinuxServer {
        LinuxServer {
            port: port,
            process_request_callback: process_request_callback,
            socket_fd: 0,
            socket_backlog: 128
        }
    }

    pub fn start(&mut self) {
        self.create_socket();
        LinuxServer::configure_socket_non_blocking(self.socket_fd);
        self.bind_socket();
        let _listen_result = unsafe { libc::listen(self.socket_fd, self.socket_backlog) };
        self.enter_event_poll_loop();
    }

    fn create_socket(&mut self) {
        self.socket_fd = unsafe { libc::socket(i32::from(libc::AF_INET), libc::SOCK_STREAM, 0) };
        if self.socket_fd < 0 {
            panic!("Failed to create socket!");
        }
        log::info!("Created socket on port [{}]. socket_fd: [{}]", self.port, self.socket_fd);
    }

    fn configure_socket_non_blocking(socket_fd: i32) {
        let mut socket_flags = unsafe { libc::fcntl(socket_fd, libc::F_GETFL, 0) };
        if socket_flags == -1 {
            panic!("Failed to get socket configurations (fcntl F_GETFL)!");
        }
        socket_flags |= libc::O_NONBLOCK;
        let configure_result = unsafe { libc::fcntl(socket_fd, libc::F_SETFL, socket_flags) };
        if configure_result == -1 {
            panic!("Failed to set socket configurations (fcntl F_SETFL)!");
        }
        log::info!("Configured socket nonbinding (fcntl). socket_flags: [{}]", socket_flags);
    }

    fn bind_socket(&mut self) {
        let fixed_port = self.port.to_be();
        let server_address = libc::sockaddr_in {
            sin_family: (libc::AF_INET as u16),
            sin_addr:   libc::in_addr {
                s_addr: libc::INADDR_ANY
            },
            sin_port: fixed_port,
            sin_zero: [0; 8]
        };
        let bind_result = unsafe {
            libc::bind(self.socket_fd,
                      &server_address as *const libc::sockaddr_in as *const libc::sockaddr,
                      std::mem::size_of::<libc::sockaddr>() as u32)
        };
        if bind_result < 0 {
            panic!("Failed to bind socket (bind)! errno: [{}]", io::Error::last_os_error());
        }
        log::info!("Binded socket. Fixed port: [{}]", fixed_port);
    }

    fn accept_connection(&mut self, mut event: libc::epoll_event, epoll_fd: i32) -> bool {
        let mut client_address = libc::sockaddr {
            sa_family: 0,
            sa_data: [0; 14]
        };
        let client_address_length = std::mem::size_of::<libc::sockaddr>();
        let client_fd = unsafe { libc::accept(self.socket_fd, &mut client_address, &mut(client_address_length as u32)) };
        // TODO: figure out rust errno
        // if accept_result == -1 {
        //     let errno = io::Error::last_os_error().raw_os_error();
        //     if errno == ffi::EAGAIN || errno == ffi::EWOULDBLOCK {
        //         // done processing incoming connection.
        //     }
        //     else {
        //         panic!("Failed to accept incoming event! errno: [{}]", io::Error::last_os_error());
        //     }
        // }
        if client_fd == -1 {
            return false;
        }

        // TODO: translate client sockaddr "name" getnameinfo.

        LinuxServer::configure_socket_non_blocking(client_fd);

        event.u64 = client_fd as u64;
        event.events = libc::EPOLLIN as u32 | libc::EPOLLET as u32;
        let epoll_control_result = unsafe {
            libc::epoll_ctl(epoll_fd, libc::EPOLL_CTL_ADD, client_fd, &mut event) 
        };
        if epoll_control_result == -1 {
            panic!("Failed to configure client connection epoll_ctl! errno: [{}]", io::Error::last_os_error());
        }

        return true;
    }

    fn process_event(&mut self, event_fd: i32) -> bool {
        let mut buffer: [std::os::raw::c_char; 16384] = [0; 16384];
        let buffer_pointer: *mut core::ffi::c_void = &mut buffer as *mut _ as *mut core::ffi::c_void;
        let read_count = unsafe { libc::read(event_fd, buffer_pointer, 16384) };

        // TODO: if read count = 16k, read more

        log::info!("process_event. read_count: [{}]", read_count);

        if read_count == -1 { // read all data
            if io::Error::last_os_error().raw_os_error().unwrap() == libc::EAGAIN {
                return false;
            }
        }
        
        else if read_count == 0 { // EOF - remote closed connection
            log::info!("Closing connection. [{}]", event_fd);
            unsafe { libc::close(event_fd) };
            return false;
        }

        let request = crate::Request::new(&buffer);
    
        let request_string = std::str::from_utf8(
            unsafe {
                &*(&buffer as *const [i8] as *const [u8])
            }
        ).unwrap();

        log::info!("request raw string: {}", request_string);
        log::info!("request: [{:?}]", request);
        

        // Call the user's callback to generate the response.
        let mut response = (self.process_request_callback)(request);
        response.write_response();

        #[cfg(debug_assertions)]
        {
            log::debug!("Response Bytes: {:?}", response.data);
            let response_string = match std::str::from_utf8(&response.data) {
                Ok(v) => v,
                _ => "Failed to convert data to utf-8. It may be binary data."
            };
            log::debug!("Response:\n{}", response_string);
        }

        let mut write_buffer: [u8; 16384] = [0; 16384];
        write_buffer[..response.data.len()].copy_from_slice(&response.data);
        let response_buffer_pointer: *mut core::ffi::c_void = &mut write_buffer as *mut _ as *mut core::ffi::c_void;
        
        //let response_buffer_pointer: *mut core::ffi::c_void = &mut response.data as *mut _ as *mut core::ffi::c_void;
        // TODO: send/write response.
        let write_count = unsafe { 
            libc::write(event_fd, response_buffer_pointer, response.data.len())
        };
        log::info!("response.data.len(): {}, write_count: {}", response.data.len(), write_count);

        return true;
    }

    fn enter_event_poll_loop(&mut self) {
        // Create the epoll (event poll) queue/file handler.
        // The epoll create argument is obsolete, but must be above 0 for backwards compatibility.
        let epoll_fd = unsafe { libc::epoll_create(1) }; // TODO: create vs create1?
        if epoll_fd < 0 {
            panic!(io::Error::last_os_error());
        }

        // Add our web server listener socket to the event poll "interest list".
        let mut event = libc::epoll_event {
            events: (libc::EPOLLIN | libc::EPOLLONESHOT) as u32,
            u64: self.socket_fd as u64,
        };
        let control_result = unsafe {
            libc::epoll_ctl(epoll_fd, libc::EPOLL_CTL_ADD, self.socket_fd, &mut event) 
        };
        if control_result < 0 {
            panic!(io::Error::last_os_error()); // I'm not sure if the os throws an error here. TODO: test and add an error log.
        }

        // Enter the master while loop.
        log::info!("Entering main event loop.");
        let mut events = Vec::with_capacity(10);
        loop {
            //int n = epoll_wait(epoll_fd, events->data(), Server::MAX_EVENTS, -1);
            // Get all events (network requests in this case) ready to be processed.
            let ready_file_count = unsafe { libc::epoll_wait(epoll_fd, events.as_mut_ptr(), 10, -1) };
            unsafe { events.set_len(ready_file_count as usize); }

            log::info!("Ready events: [{}]", ready_file_count);

            if ready_file_count < 0 {
                panic!(io::Error::last_os_error());
            }

            // events
            for i in 0..(ready_file_count as usize) {
                log::info!("Processing event: [{}]", i);
                unsafe { log::info!("events[i].events: [{}]", events[i].events); }
                if events[i].events & (libc::EPOLLERR as u32) == 1
                    || events[i].events & (libc::EPOLLHUP as u32) == 1 {
                    if events[i].events & (libc::EPOLLERR as u32) == 1 {
                        log::error!("Events data returned EPOLLERR");
                    }
                    if events[i].events & (libc::EPOLLHUP as u32) == 1 {
                        log::error!("Events data returned EPOLLHUP");
                    }
                    unsafe { panic!("Epoll event error. events: [{}]", events[i].events); }
                }
                if events[i].events & (libc::EPOLLIN as u32) == 1 {
                    unsafe { log::info!("Socket connected. fd = [{}]", events[i].u64); }
                }
                if self.socket_fd == (events[i].u64 as i32) {
                    while self.accept_connection(events[i], epoll_fd) {
                        // do nothing
                    }
                }
                else {
                    // TODO: process the event (callback to owner).
                    while self.process_event(events[i].u64 as i32) {
                        // do nothing
                    }
                }
                //println!("{}", i);
            }
        }
    }
}
